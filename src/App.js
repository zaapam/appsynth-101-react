import React, { Component } from 'react';
import { Form, Container, Button, Alert } from 'react-bootstrap';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputVal: '',
      errorMsg: null,
      data: null,
    };

    this.updateInputValue = this.updateInputValue.bind(this)
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    const errorMsg = this.isUrlValid(this.state.inputVal) ? null : "URL is not correct"


    if (!errorMsg) {
      fetch(`http://localhost:8000/short/get?url=${this.state.inputVal}`).then(result => {
        return result.json()
      }).then(data => {
        this.setState({
          errorMsg: errorMsg,
          data: data,
        })
      })
    }

    this.setState({
      errorMsg: errorMsg
    })
  }

  updateInputValue(e) {
    this.setState({
      inputVal: e.target.value
    });
  }

  isUrlValid(url) {
    const res = url.match(/http(s)?:\/\/.?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res == null)
      return false;
    else
      return true;
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>SHORTEN URL SERVICE</p>
        </header>
        <Container>
          <Form>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Alert variant="primary" style={{ marginTop: "10px" }}>
                <Alert.Heading>PUT YOUR URL</Alert.Heading>
              </Alert>
              <Form.Control type="email" placeholder="e.g. https://www.google.com" onChange={this.updateInputValue} />
            </Form.Group>
          </Form>
          <Button variant="primary" size="lg" block onClick={this.handleClick}>
            GET SHORTEN
          </Button>
          {this.state.data && (<Alert variant="success" style={{ marginTop: "10px" }}>
            {this.state.data.shorten_url}
          </Alert>)}
          {this.state.errorMsg && (<Alert variant="danger" style={{ marginTop: "10px" }}>
            {this.state.errorMsg}
          </Alert>)}
        </Container>
      </div>
    );
  }
}

export default App;
